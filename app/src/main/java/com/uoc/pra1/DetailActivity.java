package com.uoc.pra1;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.uoc.datalevel.DataException;
import com.uoc.datalevel.DataObject;
import com.uoc.datalevel.DataQuery;
import com.uoc.datalevel.FindCallback;
import com.uoc.datalevel.GetCallback;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);



        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");



        // ************************************************************************
        // UOC - BEGIN - CODE6
        //

        // Pregunta 4.1
        // Rebem el object_id de Intent
        String object_id = getIntent().getStringExtra("object_id");

        final DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null) {

                    // Pregunta 4.3

                    // creem els contenidors vinculats amb el layout i els carreguem les dades
                    TextView name = (TextView)findViewById(R.id.name);
                    name.setText((String) object.get("name"));

                    String euroUnicode = "\u20AC";
                    TextView price = (TextView)findViewById(R.id.price);
                    price.setText(((String) object.get("price"))+ euroUnicode);

                    TextView description = (TextView)findViewById(R.id.description);
                    description.setText((String) object.get("description"));

                    ImageView  thumbnail=  (ImageView)findViewById(R.id.thumbnail);
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));

                } else {
                    // Error

                }
            }
        });
        // UOC - END - CODE6
        // ************************************************************************





    }
}
